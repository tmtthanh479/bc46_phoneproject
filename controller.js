var filteredProducts = []; // Lưu trữ danh sách sản phẩm đã lọc
function renderProductList(productArr) {
  var contentHTML = "";
  productArr.forEach(function (item) {
    //  forEach(function (item) là đi duyệt hàm
    // item đại diện cho từng phần tử trong productArr
    var content = `
    <div class="max-w-xs w-full bg-white shadow-md border-2 border-gray-100 py-10 rounded-lg overflow-hidden m-4 hover:scale-[1.05] transition-transform duration-300 ease-in-out">
    <img src="${item.img}" alt="${item.name}" class="cart-item-image">
    <div class="p-4">
      <h2 id="${item.name}" class="font-bold text-xl mb-2">${item.name}</h2>

      <div id="${item.screen}" class=" screen-screen text-black font-semibold">Screen: <span class="text-gray-600">${item.screen}</span></div>
      <div class="text-black font-semibold">Back Camera: <span class="text-gray-600">${item.backCamera}</span></div>
      <div   class="text-black font-semibold">Front Camera: <span class="text-gray-600">${item.frontCamera}</span></div>
      <div class="text-black font-semibold">Description: <span class="text-gray-600">${item.desc}</span></div>
      <div  class="text-black font-semibold">Type: <span class="text-gray-600">${item.type}</span></div>
      <div  class="text-black font-semibold">Type: <span class="text-gray-600">${item.id}</span></div>
      <div id ="${item.price}" class=" text-lg price-price text-black font-semibold">Price: <span class="text-gray-600">${item.price}$</span></div>
      <button id= "button-cart"  class="bg-black hover:bg-white hover:text-black hover:border-2 hover:border-red-300 text-white font-bold py-2 px-4 rounded mt-1">
      thêm vào giỏ hàng</button>
    </div>
  </div>
  
  

    `;
    contentHTML += content;
  });

  document.getElementById("tblDanhSachSP").innerHTML = contentHTML;
  // show thông tin lên cart
  var addToCartButtons = document.querySelectorAll("#button-cart"); // button "thêm vào giỏ hàng"
  addToCartButtons.forEach(function (button) {
    button.addEventListener("click", function () {
      // lấy thông tinh
      var productElement = this.parentNode;

      var ten = productElement.querySelector("h2");
      var gia = productElement.querySelector(".price-price");
      var manHinh = productElement.querySelector(".screen-screen");
      //  tạo object để add tất cả item( thuộc tính của phone) vào giỏ hàng
      var item = {
        name: ten.innerText,
        price: gia.innerText,
        screen: manHinh.innerText,
      };

      addToCart(item);
    });
  });
}
// funtion tăng giảm số lượng
function showCart() {
  var cartItemsHTML = [];
  var totalPrice = 0; // Biến lưu trữ tổng giá tiền
  cart.forEach(function (item) {
    var cartItemHTML = `
      <div class="cart-item text-black border-2 rounded-lg mx-1 px-2 py-2 my-2 shadow-lg">
        <p class= "font-bold"> Product: ${item.name}</p>
        <p>  ${item.price}</p> 
        <p>  ${item.screen}</p> 
        <div class="quantity-buttons flex">
          <button class="quantity-button-plus border-2 px-1 py-1 border-gray-600 transform transition-transform hover:scale-110 duration-100  ">+</button>
          <p class="quantity justify-center px-5 my-auto">${item.quantity}</p>
          <button class="quantity-button-minus border-2 px-1 py-1 border-gray-600 transform transition-transform hover:scale-110 duration-100  ">-</button>
          </div>
          <button id="${item.name}" id= "button-remove"  class="button-remove pt-2 transform transition-transform hover:scale-110 duration-100"><i class="fa-solid fa-trash-can" style="color: #000000;"></i></button>
      </div>
    `;

    cartItemsHTML.push(cartItemHTML);
    totalPrice += parseInt(item.price.replace(/[^0-9]/g, "")) * item.quantity; // Tính tổng giá tiền của từng sản phẩm
  });

  document.getElementById("total").innerHTML = "Total:$ " + totalPrice; // Hiển thị tổng giá tiền
  document.getElementById("product-cart-content").innerHTML = cartItemsHTML;
  // document.getElementById("product-cart-content").innerHTML = cartItemsHTML.join(""); // xóa dấu ","" khi render 2 sản phẩm trở lên

  // theo dõi user khi user click vào nút "+" và "-"
  var plusButtons = document.querySelectorAll(".quantity-button-plus");
  var minusButtons = document.querySelectorAll(".quantity-button-minus");

  //  thì sau đó chạy function
  plusButtons.forEach(function (button) {
    button.addEventListener("click", function () {
      // cần biết plusButtons thành array dùng Array.from(plusButtons)
      var itemIndex = Array.from(plusButtons).indexOf(button); // vì indexOf chỉ dùng được với array
      increaseQuantity(itemIndex);
    });
  });

  minusButtons.forEach(function (button) {
    button.addEventListener("click", function () {
      var itemIndex = Array.from(minusButtons).indexOf(button);
      decreaseQuantity(itemIndex);
    });
  });

  // Lắng nghe sự kiện click vào nút "remove"
  var removeButtons = document.querySelectorAll(".button-remove");
  removeButtons.forEach(function (button) {
    button.addEventListener("click", function () {
      var itemName = button.getAttribute(` "button-remove"`);
      deleteProduct(itemName); // gọi đến function id ở trên id=id= "button-remove" 
    });
  });

  console.log(cart); // Hiển thị thông tin giỏ hàng trong console
}
