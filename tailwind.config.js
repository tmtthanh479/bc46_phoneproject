module.exports = {
    theme: {
      extend: {
        colors: {
          primary: '#3490dc',
          secondary: '#ffed4a',
        },
        fontSize: {
          '2xl': '1.5rem',
        },
        width: {
            "101" : "300px"
        }
      },
    },
    // ...other configurations
  }
  